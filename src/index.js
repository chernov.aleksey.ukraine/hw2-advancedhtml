document.addEventListener("click", (event) => {
  if (
    event.target.classList.contains("header__burger-menu-button") &&
    document
      .querySelector(".header__menu")
      .classList.contains("header__menu--disabled")
  ) {
    document
      .querySelector(".header__menu")
      ?.classList.add("header__menu--active");
    document
      .querySelector(".header__menu")
      ?.classList.remove("header__menu--disabled");
    document
      .querySelector(".header__burger-menu-button")
      ?.classList.add("burger-menu-button--cross");
    document
      .querySelector(".header__burger-menu-button")
      ?.classList.remove("burger-menu-button--lines");
  } else {
    document
      .querySelector(".header__menu")
      ?.classList.add("header__menu--disabled");
    document
      .querySelector(".header__menu")
      ?.classList.remove("header__menu--active");
    document
      .querySelector(".header__burger-menu-button")
      ?.classList.add("burger-menu-button--lines");
    document
      .querySelector(".header__burger-menu-button")
      ?.classList.remove("burger-menu-button--cross");
  }
});
