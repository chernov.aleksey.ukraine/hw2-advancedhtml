import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import browserSync from "browser-sync";
import concat from "gulp-concat";
import terser from "gulp-terser";
import cleancss from "gulp-clean-css";
import autoprefixer from "gulp-autoprefixer";
import imagemin from "gulp-imagemin";
import clean from "gulp-clean";

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const css = () =>
  gulp
    .src("./src/*.scss")
    .pipe(sass())
    .pipe(concat("styles.min.css"))
    .pipe(cleancss())
    .pipe(autoprefixer())
    .pipe(gulp.dest("./dist/styles"));

const js = () =>
  gulp
    .src("./src/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist/"));

const minimage = () =>
  gulp.src("./src/img/*").pipe(imagemin()).pipe(gulp.dest("./dist/img"));

const cleardist = () => gulp.src("./dist/*").pipe(clean());

const innerbuild = gulp.series(css, js);

export const build = gulp.series(cleardist, gulp.parallel(css, js, minimage));

export const dev = gulp.series(innerbuild, () => {
  BS.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch(
    "./src/**/*",
    gulp.series(innerbuild, (done) => {
      BS.reload();
      done();
    })
  );
  gulp.watch(
    "./index.html",
    gulp.series(innerbuild, (done) => {
      BS.reload();
      done();
    })
  );
});
